# Empty New Tab Page

![License badge](https://raster.shields.io/badge/license-The%20Unlicense-lightgrey.png "License badge")

![Thumbnail](thumbnail.png?raw=true "Thumbnail")

![Thumbnail2](thumbnail_2.png?raw=true "Thumbnail")

This extension overrides the new tab page with an empty page, for users who don't like the original or custom new tab pages.

Project homepage: [GitLab pages](https://ethicist.gitlab.io/emptytab/) and [Surge](https://emptytab.surge.sh/)

## Install

Extension can be loaded in unpacked mode by following the following steps:

* Download latest version of extension [emptytab_latest.zip](https://gitlab.com/Ethicist/emptytab/tree/master/release/emptytab_latest.zip)
* Unpack emptytab_latest.zip zip to another folder.
* Visit chrome://extensions (via omnibox or menu -> Tools -> Extensions).
* Enable Developer mode by ticking the checkbox in the upper-right corner.
* Click on the "Load unpacked extension..." button.
* Select the directory containing your unpacked extension.

## Contributing

Please feel free to submit pull requests.
Bugfixes and simple non-breaking improvements will be accepted without any questions.

## License

This is free and unencumbered software released into the public domain.  
For more information, please refer to the [LICENSE](LICENSE) file or [unlicense.org](https://unlicense.org).
