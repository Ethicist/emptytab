var target = document.getElementById("download");

target.onclick = function() {
	download('./release/emptytab_latest.zip');
}

function download(path) {
  var src = '' + path;
  if (src.match(/\?/)) {
    src += '&dl=1';
  } else {
    src += '?dl=1';
  }
  location.replace(src);
  return false;
}
