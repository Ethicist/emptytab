var contenteditable = document.querySelector('[contenteditable]');

function save() {
  if (localStorage) {
    localStorage.setItem('saved', contenteditable.textContent);
  }
}

function load() {
  if (localStorage) {
    contenteditable.textContent = localStorage.getItem('saved');
  }
}

document.onkeyup = function () {
  save();
}

window.onstorage = function () {
  load();
}

load();